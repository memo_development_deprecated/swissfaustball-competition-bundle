window.addEventListener('DOMContentLoaded', (event) => {
    if ($(".cup_select_league select[name='league']").length) {
        $(".cup_select_league select[name='league']").on("change", function (e) {
            if ($(this).val()) {
                $.post("/memo_competition/getTesamsByLeague/" + $(this).val(), {season: $("input[name='season']").val()}, function (data) {
                    var Sel = $('<select name="squad" required="required"></select>');
                    let tData = jQuery.parseJSON(data);
                    for (i = 0; i < tData.length; i++) {
                        Sel.append($('<option value="">' + tData[i].club + '</option>'));
                    }
                    let Wrapper = $("input[name='squad']").parent();
                    if ($("select[name='squad']").length) {
                        Wrapper = $("select[name='squad']").parent();
                        $("select[name='squad']").remove();
                    }
                    Wrapper.append(Sel);
                    $("input[name='squad']").remove();
                });
            } else {
                let Wrapper = $("input[name='squad']").parent();
                if ($("select[name='squad']").length) {
                    Wrapper = $("select[name='squad']").parent();
                    $("select[name='squad']").remove();
                }
                Wrapper.append($('<input type="text" name="squad" />'));
            }
        });
    }

    if ($("input[name='2liga']").length) {
        let elem = $("input[name='2liga']");
        elem.on('click', function (e) {
            if (e.target.checked == true) {
                $("select[name='region']").prop('required', true);
                $("input[name='squad_2_liga']").prop('required', true);
            } else {
                $("select[name='region']").prop('required', false);
                $("input[name='squad_2_liga']").prop('required', false);
            }
        });
    }

    if ($("select[name='squad']").length) {
        $("select[name='squad']").on("change", function (e) {
            $.post("/memo_competition/teamData/" + $(this).val(), {season: $("input[name='season']").val()}, function (data) {
                if (data.team !== undefined) {
                    $("input[name='league']").val(data.team);
                }

                if ($("input[name='name']").length && data.contactName) {
                    $("input[name='name']").val(data.contactName);
                    $("input[name='sendername']").val(data.contactName);
                }

                if ($("input[name='prename']").length && data.contactFirstname) {
                    $("input[name='prename']").val(data.contactFirstname);
                    $("input[name='senderprename']").val(data.contactFirstname);
                }

                if ($("input[name='street']").length && data.contactAddress) {
                    $("input[name='street']").val(data.contactAddress);
                }

                if ($("input[name='location']").length && data.contactZipcode) {
                    $("input[name='location']").val(data.contactZipcode + " " + data.contactPlace);
                }
                if ($("input[name='phoneprivate']").length && data.contactPhone) {
                    $("input[name='phoneprivate']").val(data.contactPhone);
                }
                if ($("input[name='phonecompany']").length && data.contactPhoneBusiness) {
                    $("input[name='phonecompany']").val(data.contactPhoneBusiness);
                }
                if ($("input[name='mobilephone']").length && data.contactMobile) {
                    $("input[name='mobilephone']").val(data.contactMobile);
                }
                if ($("input[name='email']").length && data.contactEmail) {
                    $("input[name='email']").val(data.contactEmail);
                    $("input[name='senderemail']").val(data.contactEmail);
                }

                if (data.coach !== 'undefined') {
                    if ($("input[name='coachname']").length) {
                        $("input[name='coachname']").val(data.coach.name);
                    }
                    if ($("input[name='coachprename']").length) {
                        $("input[name='coachprename']").val(data.coach.firstname);
                    }
                    if ($("input[name='eduationLevel']").length) {
                        $("input[name='eduationLevel']").val(data.coach.educationLevel);
                    }
                    if ($("input[name='gymnasticassociation']").length) {
                        $("input[name='gymnasticassociation']").each(function (idx, elem) {
                            if ($(elem).val() == data.association) {
                                $(elem).attr('checked', true);
                            }
                        });
                    }
                }
            });
        });
    }

    /*Anmeldung Schulmeisterschaft*/
    var BreakException = {};
    function addFormRow(event) {
        let rowCount    = document.querySelectorAll('div.rowWrapper').length;
        let elem = document.querySelectorAll('div.rowWrapper');
        try {
            elem.forEach(e => {
                if(e.style.display == 'none'){
                    $(e).fadeIn();
                    throw BreakException;
                }
            });
        } catch (e) {
            if (e !== BreakException) throw e;
        }
    }

    if($('#teams').length) {
        $(".rowWrapper").hide();
        $(".rowWrapper").first().show();
        const btnAddItem = document.createElement('a');
        const Icon = document.createElement('i');
        const linkTitle = document.createTextNode(" Team hinzufügen");
        Icon.classList.add('icon');
        Icon.classList.add('icon--plus');
        btnAddItem.setAttribute('data-add_row', '');
        btnAddItem.appendChild(Icon);
        btnAddItem.appendChild(linkTitle);
        btnAddItem.addEventListener('click', addFormRow, false);

        document.getElementById('teams').parentNode.insertBefore(btnAddItem, document.getElementById('teams'));
    }
});
