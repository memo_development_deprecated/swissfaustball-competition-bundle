<?php

$GLOBALS['TL_DCA']['tl_module']['palettes']['competition_form'] =
    '{title_legend},name,type,competition;{notification_legend},notification;'
;

$GLOBALS['TL_DCA']['tl_module']['fields']['notification'] = [
    'exclude'					=> true,
    'inputType'					=> 'select',
    'eval'						=> array('mandatory'=>true, 'tl_class'=>'w50 clr','includeBlankOption'=>true, 'multiple'=>false, 'chosen'=>true),
    'foreignKey'			    => 'tl_nc_notification.title',
    'sql'						=> "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_module']['fields']['competition'] = [
    'exclude'					=> true,
    'inputType'					=> 'select',
    'eval'						=> array('mandatory'=>false, 'tl_class'=>'w50 clr','includeBlankOption'=>true, 'multiple'=>false, 'chosen'=>true),
    'foreignKey'			    => 'tl_competition.title',
    'sql'						=> "varchar(255) NOT NULL default ''"
];
