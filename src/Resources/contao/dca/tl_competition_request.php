<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Media Motion AG
 *
 * @package   Competition Bundle
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


/**
 * Table tl_competition
 */



$GLOBALS['TL_DCA']['tl_competition_request'] = array
(
    // Config
    'config' => array
    (
        'dataContainer'    => 'Table',
		'ptable'		   => 'tl_competition',
        'switchToEdit'     => true,
        'enableVersioning' => true,
        'notCreatable'     => true,
        'sql' => [
            'keys' => [
                'id' => 'primary',
            ],
        ],
    ),
    // List
    'list' => array
    (
	    'sorting' => array
		(
		    'mode' => 0,
		    'flag' => 11,
		    'headerFields'=> array('tstamp','team','league'),
		    'panelLayout' => 'sort,filter;search,limit',
		),
		'label' => array
		(
			'fields'                  => array('tstamp','team', 'league'),
			'showColumns'			  => true,
			'format'				  => '%s',
            'label_callback'          => array('tl_competition_request', 'labelFormat')
		),
		'global_operations' => array
		(
			'export' => array
			(
				'href'                => 'key=export',
				'class'               => 'exportXls',
				'attributes'          => 'onclick="Backend.getScrollOffset()" accesskey="e"'
			)
		),
		'operations' => array
		(
            'exportPdf' => array
            (
                'href'                => 'key=exportPdf',
                'class'               => 'exportPdf',
                'attributes'          => 'onclick="Backend.getScrollOffset()" target="_blank" accesskey="p"',
                'icon'                => '/bundles/competition/img/pdf.svg'
            ),
            'show' => array
			(
				'href'                => 'act=show',
				'icon'                => 'show.svg'
			)
		)
    ),

    // Palettes
    'palettes' => array
    (
        '__selector__'				  => [],
        'default'                     => '{team_legend},team,league;',
    ),

    // Subpalettes
    'subpalettes' => array
    (

    ),

    // Fields
    'fields' => array
    (
        'id' => array
        (
            'label'                   => array('ID'),
            'search'                  => false,
            'sql'                     => "int(10) unsigned NOT NULL auto_increment"
        ),
		'pid' => array
		(
			'foreignKey'				=> 'tl_competition.id',
			'relation'					=> array('type'=>'belongsTo', 'load'=>'lazy'),
			'sql'						=> "int(10) unsigned NOT NULL "
		),
        'sorting' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default 0"
        ),
        'tstamp' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default 0"
        ),
        'team' => array
        (
	        'sorting'				  => true,
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>true, 'maxlength'=>255, 'tl_class'=>'w50'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
		'league' => array
		(
			'search'                  => true,
			'inputType'           	  => 'text',
			'eval'                    => array('mandatory'=>false, 'maxlength' => 255, 'tl_class' => 'w50', 'unique' => true),
			'sql'                 	  => "varchar(255) NOT NULL default ''",
		),
		'formData' => array
		(
			'exclude'                 => true,
			'inputType'               => 'text',
			'sql'                     => "blob NULL"
		),
    )
);


/**
 * Class tl_mod_filecategory
 * Definition der Callback-Funktionen für das Datengefäss.
 */

class tl_competition_request extends Backend
{

    /**
     * @param array $row
     * @param string $label
     * @param DataContainer $dc
     * @param array $labels
     * @return array
     */
    public function labelFormat(array $row, string $label,DataContainer $dc,array $labels):array
    {
        $labels[0] = strftime("%d.%m.%Y %H:%M",$labels[0]);

        //Hack remove empty label
        if(empty($labels[3]))
        {
            unset($labels[3]);
        }
        return $labels;
    }
}
