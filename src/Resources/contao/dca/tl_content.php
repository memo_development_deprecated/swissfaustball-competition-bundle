<?php




$GLOBALS['TL_DCA']['tl_content']['palettes']['MemoCompetitionButton'] = '
    {type_legend},type;
    {button_legend},competition,buttonTitle,jumpTo,target;
    {template_legend:hide},customTpl;
    {protected_legend:hide},protected;
    {expert_legend:hide},guests,cssID;
    {invisible_legend:hide},invisible,start,stop;
';

$GLOBALS['TL_DCA']['tl_content']['fields']['competition'] = array(
    'exclude'                 => true,
    'search'                  => true,
    'inputType'               => 'select',
    'foreignKey'              => 'tl_competition.title',
    'eval'                    => ['tl_class'=>'w50'],
    'sql'                     => "varchar(255) NOT NULL default ''"
);



$GLOBALS['TL_DCA']['tl_content']['fields']['buttonTitle'] = array
(
    'exclude'                 => true,
    'search'                  => true,
    'inputType'               => 'text',
    'eval'                    => array('maxlength'=>255, 'tl_class'=>'w50'),
    'sql'                     => "varchar(255) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_content']['fields']['jumpTo'] = array
(
    'exclude'                 => true,
    'inputType'               => 'pageTree',
    'foreignKey'              => 'tl_page.title',
    'eval'                    => array('fieldType'=>'radio','mandatory'=>false, 'tl_class'=>'w50 m12 clr'), // do not set mandatory (see #5453)
    'sql'                     => "int(10) unsigned NOT NULL default 0",
    'relation'                => array('type'=>'hasOne', 'load'=>'lazy')
);


?>
