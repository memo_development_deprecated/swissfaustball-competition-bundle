<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Media Motion AG
 *
 * @package   Competition Bundle
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


/**
 * Table tl_competition
 */



$GLOBALS['TL_DCA']['tl_competition'] = array
(
    // Config
    'config' => array
    (
        'dataContainer'    => 'Table',
		'ctable'		   => array('tl_competition_request'),
        'switchToEdit'     => true,
        'enableVersioning' => true,
        'markAsCopy'       => 'title',
        'sql' => [
            'keys' => [
                'id' => 'primary',
                'alias' => 'index',
            ],
        ],
    ),
    // List
    'list' => array
    (
	    'sorting' => array
		(
		    'mode' => 0,
		    'flag' => 11,
		    'headerFields'=> array('title'),
		    'panelLayout' => 'sort,filter;search,limit',
		),
		'label' => array
		(
			'fields'                  => array('title'),
			'showColumns'			  => true,
			'format'				  => '%s',
            'label_callback'          => ['tl_competition','getListLabels']
		),
		'global_operations' => array
		(
			'all' => array
			(
				'href'                => 'act=select',
				'class'               => 'header_edit_all',
				'attributes'          => 'onclick="Backend.getScrollOffset()" accesskey="e"'
			)
		),
		'operations' => array
		(
			'editheader' => array
			(
				'href'                => 'act=edit',
				'icon'                => 'edit.svg'
			),
			'copy' => array
			(
				'href'                => 'act=copy',
				'icon'                => 'copy.svg'
			),
			'delete' => array
			(
				'href'                => 'act=delete',
				'icon'                => 'delete.svg',
				'attributes'          => 'onclick="if(!confirm(\'wirklich löschen?\'))return false;Backend.getScrollOffset()"'
			),
			'toggle' => array
			(
				'icon'                => 'visible.svg',
				'attributes'          => 'onclick="Backend.getScrollOffset();return AjaxRequest.toggleVisibility(this,%s)"',
				'button_callback'     => array('tl_competition', 'toggleIcon'),
				'showInHeader'        => true
			),
			'show' => array
			(
				'href'                => 'act=show',
				'icon'                => 'show.svg'
			),
			'editRequest' => array
			(
				'label'					=> &$GLOBALS['TL_LANG']['tl_competition']['editRequest'],
				'href'					=> 'table=tl_competition_request',
				'icon'					=> '/bundles/competition/img/envelope.gif'
			)
		)
    ),

    // Palettes
    'palettes' => array
    (
        '__selector__'				  => [],
        'default'                     => '{title_legend},title,alias;{form_legend},seasonSelect,leagueSelect,sex,formRegarding,formReceiver,formText,formSelect;{jumpTo_legend:hide},jumpTo;{expert_legend:hide},exportRaw,exportTpl;{publish_legend},published,start,stop;',
    ),

    // Subpalettes
    'subpalettes' => array
    (

    ),

    // Fields
    'fields' => array
    (
        'id' => array
        (
            'label'                   => array('ID'),
            'search'                  => true,
            'sql'                     => "int(10) unsigned NOT NULL auto_increment"
        ),
        'sorting' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default 0"
        ),
        'tstamp' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default 0"
        ),
        'title' => array
        (
	        'sorting'				  => true,
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>true, 'maxlength'=>255, 'tl_class'=>'w50'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
		'alias' => array
		(
			'search'                  => true,
			'inputType'           	  => 'text',
			'eval'                    => array('mandatory'=>false, 'maxlength' => 255, 'tl_class' => 'w50', 'unique' => true),
			'save_callback' => array
			(
				array('tl_competition', 'generateAlias')
			),
			'sql'                 	  => "varchar(255) NOT NULL default ''",
		),
        'formRegarding' => array
        (
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>true, 'maxlength'=>255, 'tl_class'=>'w50 clr'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'formReceiver' => array
        (
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>true, 'maxlength'=>255, 'tl_class'=>'w50', 'rgxp'=>''),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'formText' => array
		(
			'exclude'					=> true,
			'inputType'					=> 'textarea',
			'eval'						=> array('mandatory'=>false, 'rte'=>'tinyMCE', 'tl_class'=>'clr'),
			'sql'						=> "mediumtext NULL"
		),
        'formSelect' => array
		(
			'exclude'					=> true,
			'inputType'					=> 'checkboxWizard',
			'eval'						=> array('mandatory'=>true, 'tl_class'=>'clr', 'multiple'=>true, 'chosen'=>true),
			'options_callback'			=> ['tl_competition', 'getFormOptions'],
			'sql'						=> "varchar(255) NOT NULL default ''",
		),
        'leagueSelect' => array
		(
			'exclude'					=> true,
			'inputType'					=> 'select',
			'eval'						=> array('mandatory'=>false, 'tl_class'=>'w50', 'chosen'=>true, 'multiple'=>true, 'includeBlankOption'=>true),
            'options'                   => ['a'=>'Nationalliga A','b'=>'Nationalliga B','c'=>'1. Liga'],
			'sql'						=> "varchar(255) NOT NULL default ''",
		),
        'seasonSelect' => array
        (
            'exclude'					=> true,
            'inputType'					=> 'select',
            'eval'						=> array('mandatory'=>false, 'tl_class'=>'w50', 'chosen'=>true, 'includeBlankOption'=>true,'isAssociative'=>true),
            'options'                   => [0=>'Halle',1=>'Feld'],
            'sql'						=> "varchar(255) NOT NULL default ''",
        ),
        'sex' => array
		(
			'exclude'					=> true,
			'inputType'					=> 'select',
			'eval'						=> array('mandatory'=>false, 'tl_class'=>'w50', 'chosen'=>true, 'includeBlankOption'=>true),
            'options'               	=> array('male' => 'Männer', 'female' => 'Frauen'),
			'sql'						=> "varchar(255) NOT NULL default ''",
		),
        'jumpTo' => array
        (
            'exclude'                 => true,
            'inputType'               => 'pageTree',
            'foreignKey'              => 'tl_page.title',
            'eval'                    => array('fieldType'=>'radio','mandatory'=>false), // do not set mandatory (see #5453)
            'sql'                     => "int(10) unsigned NOT NULL default 0",
            'relation'                => array('type'=>'hasOne', 'load'=>'lazy')
        ),
        'exportRaw' => array
        (
            'sorting'				  => false,
            'exclude'                 => true,
            'filter'                  => false,
            'inputType'               => 'checkbox',
            'eval'                    => array('doNotCopy'=>true),
            'sql'                     => "char(1) NOT NULL default ''"
        ),
        'exportTpl' => array
        (
            'exclude'                 => true,
            'inputType'               => 'select',
            'options_callback' => static function ()
            {
                return Controller::getTemplateGroup('memo_competition_export_');
            },
            'eval'                    => array('includeBlankOption'=>true, 'chosen'=>true, 'tl_class'=>'w50'),
            'sql'                     => "varchar(64) COLLATE ascii_bin NOT NULL default ''"
        ),
        'published' => array
        (
	        'sorting'				  => true,
            'exclude'                 => true,
            'filter'                  => true,
            'inputType'               => 'checkbox',
            'eval'                    => array('doNotCopy'=>true),
            'sql'                     => "char(1) NOT NULL default ''"
        ),
        'start' => array
        (
            'exclude'                 => true,
            'inputType'               => 'text',
            'eval'                    => array('rgxp'=>'datim', 'datepicker'=>true, 'tl_class'=>'w50 wizard'),
            'sql'                     => "varchar(10) NOT NULL default ''"
        ),
        'stop' => array
        (
            'exclude'                 => true,
            'inputType'               => 'text',
            'eval'                    => array('rgxp'=>'datim', 'datepicker'=>true, 'tl_class'=>'w50 wizard'),
            'sql'                     => "varchar(10) NOT NULL default ''"
        )
    )
);


/**
 * Class tl_mod_filecategory
 * Definition der Callback-Funktionen für das Datengefäss.
 */
class tl_competition extends Backend
{

	/**
	 * Import the back end user object
	 */
	public function __construct()
	{
		parent::__construct();
		$this->import(BackendUser::class, 'User');
	}

	public function generateAlias($varValue, DataContainer $dc)
	{
		$autoAlias = false;
		// Generate an alias if there is none
		if ($varValue == '')
		{
			$autoAlias = true;
			$varValue = StringUtil::generateAlias($dc->activeRecord->title);
		}

		$objAlias = $this->Database->prepare("SELECT id FROM tl_competition WHERE id=? OR alias=?")
								   ->execute($dc->id, $varValue);
		// Check whether the page alias exists
		if ($objAlias->numRows > 1)
		{
			if (!$autoAlias)
			{
				throw new Exception(sprintf($GLOBALS['TL_LANG']['ERR']['aliasExists'], $varValue));
			}
			$varValue .= '-' . $dc->id;
		}
		return $varValue;
	}

	/**
	 * Return the "toggle visibility" button
	 *
	 * @param array  $row
	 * @param string $href
	 * @param string $label
	 * @param string $title
	 * @param string $icon
	 * @param string $attributes
	 *
	 * @return string
	 */
	public function toggleIcon($row, $href, $label, $title, $icon, $attributes)
	{
		if (Input::get('tid'))
		{
			$this->toggleVisibility(Input::get('tid'), (Input::get('state') == 1), (func_num_args() <= 12 ? null : func_get_arg(12)));
			$this->redirect($this->getReferer());
		}

		// Check permissions AFTER checking the tid, so hacking attempts are logged
		if (!$this->User->hasAccess('tl_competition::published', 'alexf'))
		{
			return '';
		}

		$href .= '&amp;tid=' . $row['id'] . '&amp;state=' . ($row['published'] ? '' : 1);

		if (!$row['published'])
		{
			$icon = 'invisible.svg';
		}

		return '<a href="' . $this->addToUrl($href) . '" title="' . StringUtil::specialchars($title) . '"' . $attributes . '>' . Image::getHtml($icon, $label, 'data-state="' . ($row['published'] ? 1 : 0) . '"') . '</a> ';
	}


	/**
	 * Disable/enable a user group
	 *
	 * @param integer       $intId
	 * @param boolean       $blnVisible
	 * @param DataContainer $dc
	 */
	public function toggleVisibility($intId, $blnVisible, DataContainer $dc=null)
	{
		// Set the ID and action
		Input::setGet('id', $intId);
		Input::setGet('act', 'toggle');

		if ($dc)
		{
			$dc->id = $intId; // see #8043
		}

		// Check the field access
		if (!$this->User->hasAccess('tl_competition::published', 'alexf'))
		{
			throw new AccessDeniedException('Not enough permissions to publish/unpublish news item ID ' . $intId . '.');
		}

		$objRow = $this->Database->prepare("SELECT * FROM tl_competition WHERE id=?")
								 ->limit(1)
								 ->execute($intId);

		if ($objRow->numRows < 1)
		{
			throw new AccessDeniedException('Invalid competition item ID ' . $intId . '.');
		}

		// Set the current record
		if ($dc)
		{
			$dc->activeRecord = $objRow;
		}

		$objVersions = new Versions('tl_competition', $intId);
		$objVersions->initialize();

		// Trigger the save_callback
		if (is_array($GLOBALS['TL_DCA']['tl_competition']['fields']['published']['save_callback'] ?? null))
		{
			foreach ($GLOBALS['TL_DCA']['tl_competition']['fields']['published']['save_callback'] as $callback)
			{
				if (is_array($callback))
				{
					$this->import($callback[0]);
					$blnVisible = $this->{$callback[0]}->{$callback[1]}($blnVisible, $dc);
				}
				elseif (is_callable($callback))
				{
					$blnVisible = $callback($blnVisible, $dc);
				}
			}
		}

		$time = time();

		// Update the database
		$this->Database->prepare("UPDATE tl_competition SET tstamp=$time, published='" . ($blnVisible ? '1' : '') . "' WHERE id=?")
					   ->execute($intId);

		if ($dc)
		{
			$dc->activeRecord->tstamp = $time;
			$dc->activeRecord->published = ($blnVisible ? '1' : '');
		}

		$objVersions->create();

		// The onsubmit_callback has triggered scheduleUpdate(), so run generateFeed() now
		$this->generateFeed();

		if ($dc)
		{
			$dc->invalidateCacheTags();
		}
	}

    /**
     * Get Forms with checkmark competition = true
     * @param $dc
     * @return array
     */
	public function getFormOptions($dc) {

		$oForms = FormModel::findBy(["competitionForm=?"], [1]);
		$arrResult = [];

		if(empty($oForms))
        {
            return [];
        }
		foreach($oForms as $key => $value) {
			$arrResult[$value->id] = $value->title;
		}

		return $arrResult;
	}

    /**
     * Count Requests
     * @param array $row
     * @param string $label
     * @param DataContainer $dc
     * @return string
     */
    public function getListLabels(array $row, string $label, DataContainer $dc) {
        $oCompRequestsCount = \Memo\CompetitionBundle\Model\CompetitionRequestModel::countBy('pid',$row['id']);
        return sprintf('<span class="comp_badge">%02d</span> %s',$oCompRequestsCount,$label);
    }

    /**
     * @param DataContainer $dc
     * @return array
     */
    public function getSeasonOptions(DataContainer $dc) {
        $aOptions = [];
        $oSeason = Memo\ModSwissfaustballBundle\Model\SfSeasonModel::findBy(["archive=?","active=?"], ['',1],['order'=>'season DESC']);

        if($oSeason) {
            foreach($oSeason as $key => $season) {
                $strType = ($season->type == 1)? 'Feld' : 'Halle';
                $aOptions[$season->id] = sprintf("%s | %s",$season->season,$strType);
            }
        }

        return $aOptions;
    }
}
