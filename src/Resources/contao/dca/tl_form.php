<?php
	
use Contao\CoreBundle\DataContainer\PaletteManipulator;

PaletteManipulator::create()
	// apply the field "custom_field" after the field "username"
    ->addField('competitionForm', 'jumpTo')
    ->applyToPalette('default', 'tl_form');
	
$GLOBALS['TL_DCA']['tl_form']['fields']['competitionForm'] = array
(
	'exclude'                 => true,
	'filter'                  => true,
	'inputType'               => 'checkbox',
	'eval'                    => array('tl_class' => 'w50 clr m12'),
	'sql'                     => "char(1) NOT NULL default ''"
);