<?php declare(strict_types=1);

/**
 * @package   Swissfaustball Competition Bundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

/**
 * Add back end modules
 */
array_insert($GLOBALS['BE_MOD']['memo_competition'], 100, array
(
	'competitions' => array
	(
		'tables'       => array(
			'tl_competition',
			'tl_competition_request',
		),
        'export' => ['Memo\CompetitionBundle\Service\ExportService','exportRequestData'],
        'exportPdf' => ['Memo\CompetitionBundle\Service\ExportService','exportRequestPDF']
	)
));

/**
 * Add front end modules and Custom Elements
 */
$GLOBALS['TL_CTE']['MEMO Customs']['MemoCompetitionButton']  = 'Memo\CompetitionBundle\Elements\MemoCompetitionButton';
$GLOBALS['FE_MOD']['sfcompetition']['competition_form']      = 'Memo\CompetitionBundle\Module\CompetitionFormModule';


/**
 * Models
 */
$GLOBALS['TL_MODELS']['tl_competition']   = 'Memo\CompetitionBundle\Model\CompetitionModel';
$GLOBALS['TL_MODELS']['tl_competition_request']   = 'Memo\CompetitionBundle\Model\CompetitionRequestModel';


/**
 * Backend CSS
 */
if(TL_MODE == 'BE')
{
    $GLOBALS['TL_CSS'][]        = '/bundles/competition/css/backend.css';
}
