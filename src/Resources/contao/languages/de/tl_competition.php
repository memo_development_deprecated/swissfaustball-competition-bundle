<?php

$GLOBALS['TL_LANG']['tl_competition']['editRequest'] = 'Anfragen zu Ausschreibung anschauen.';

// Legends

$GLOBALS['TL_LANG']['tl_competition']['title_legend']	= 'Titelbereich';
$GLOBALS['TL_LANG']['tl_competition']['form_legend']	= 'Formularbereich';
$GLOBALS['TL_LANG']['tl_competition']['publish_legend']	= 'Veröffentlichung';
$GLOBALS['TL_LANG']['tl_competition']['jumpTo_legend']	= 'Alternative OK - Seite';


// Fields

$GLOBALS['TL_LANG']['tl_competition']['title']			= ['Titel'];
$GLOBALS['TL_LANG']['tl_competition']['alias']			= ['Alias'];
$GLOBALS['TL_LANG']['tl_competition']['leagueSelect']	= ['Liga'];
$GLOBALS['TL_LANG']['tl_competition']['seasonSelect']	= ['Saison'];
$GLOBALS['TL_LANG']['tl_competition']['formRegarding']	= ['E-Mail Betreff'];
$GLOBALS['TL_LANG']['tl_competition']['formReceiver']	= ['E-Mail Empfänger Wettbewerbsverantwortlicher','Mehrfachempfänger möglich. Bitte E-Mail Adressen mit Semikolon (;) trennen.'];
$GLOBALS['TL_LANG']['tl_competition']['formText']		= ['E-Mail Text'];
$GLOBALS['TL_LANG']['tl_competition']['formSelect']		= ['Formular-Blöcke', 'Setzen Sie das Formular durch "Blöcke" zusammen.'];
$GLOBALS['TL_LANG']['tl_competition']['sex']			= ['Geschlecht'];
$GLOBALS['TL_LANG']['tl_competition']['jumpTo']			= ['OK-Seite wählen','Alternativ können Sie eine eigene OK-Page auswählen? (Seite welche nach dem Versand angezeigt wird.)'];
$GLOBALS['TL_LANG']['tl_competition']['published']		= ['Wettbewerbsformular Veröffentlichen'];
$GLOBALS['TL_LANG']['tl_competition']['start']		= ['Startdatum'];
$GLOBALS['TL_LANG']['tl_competition']['stop']		= ['Enddatum'];
$GLOBALS['TL_LANG']['tl_competition']['expert_legend']		= 'Experteneinstellungen';
$GLOBALS['TL_LANG']['tl_competition']['exportRaw']		= ['XLS Export aller Felder erzwingen.','Alle Formularfelder in einer Spalte.'];
$GLOBALS['TL_LANG']['tl_competition']['exportTpl']		= ['XLS Export Template',''];

