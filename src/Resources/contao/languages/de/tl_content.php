<?php


$GLOBALS['TL_LANG']['tl_content']['button_legend']		= 'Button Einstellungen';
$GLOBALS['TL_LANG']['tl_content']['competition']		= ['Wettbewerbsformular'];
$GLOBALS['TL_LANG']['tl_content']['buttonTitle']		= ['Button Titel'];
$GLOBALS['TL_LANG']['tl_content']['btnStyle']		    = ['Darstellung'];
$GLOBALS['TL_LANG']['tl_content']['jumpTo']		        = ['Zielseite für Formular','Beinhaltet das Frontend-Modul für die Formularausgabe.'];
