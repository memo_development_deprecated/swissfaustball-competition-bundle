<?php

namespace Memo\CompetitionBundle\Service;

use Contao\BackendUser;
use Contao\CoreBundle\Framework\ContaoFramework;
use Contao\Model;
use Contao\StringUtil;
use Memo\CompetitionBundle\Model\CompetitionRequestModel;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\HeaderFooter;
use PhpOffice\PhpSpreadsheet\Worksheet\HeaderFooterDrawing;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Symfony\Component\HttpFoundation\Response;
use Memo\CompetitionBundle\Classes\PDF;
use Symfony\Component\HttpFoundation\RequestStack;

class ExportService
{
    /**
     * @var ContaoFramework
     */
    private $framework;

    /**
     * @var int 1st. col width
     */
    private $col1Width = 41;

    private $VerticalSpacer = 4;

    private $oComp;

    private $oCompRequest;

    private $fontSize = 10;

    private $tmpSavePath = '/files/export/';

    private $request;

    /**
     * @param ContaoFramework $framework
     * @param RequestStack $requestStack
     */
    public function __construct(ContaoFramework $framework,RequestStack $requestStack)
    {
        $this->framework = $framework;
        $this->request  = $requestStack;
        $this->framework->initialize();
    }


    /**
     * @param \DataContainer|null $dc
     * @param null $prmRequestId
     * @param false $blnSavePdf
     * @return false|Response
     */
    public function exportRequestPDF(\DataContainer $dc = NULL,$prmRequestId=NULL,$blnSavePdf = false) {

        /*$objUser = BackendUser::getInstance();
        $objUser->authenticate();
        /*if($objUser->isAdmin === false) {
            return false;
        }*/

        //Set Request ID
        if(!is_null($dc) and empty($prmRequestId)){
            $prmRequestId = $dc->id;
        }

        //Get Request Data
        $this->oCompRequest = CompetitionRequestModel::findByPk($prmRequestId);
        $this->oComp = $this->oCompRequest->getRelated('pid');

        $this->RequestData    = unserialize($this->oCompRequest->formData);

        if(empty($this->RequestData)) {
            return false;
        }

        $aForms = unserialize($this->oComp->formSelect);

        $pdf = new PDF();
        $prmPadding = 10;
        $pdf->AddPage();
        $pdf->SetMargins(10, 0, 10);
        $pdf->SetAutoPageBreak(true,30);
        $pdf->SetFont('Arial', '', 15);
        $pRight = $pdf->GetPageWidth() - $prmPadding;

        //Set Title
        $pdf->SetFillColor(230, 230, 230);
        $pdf->Cell(0, 7, utf8_decode($this->oComp->title), 0, 1, 'L', true);
        $pdf->SetFont('Arial', 'B', 15);
        $pdf->Cell(0, 7, 'Anmeldung', 0, 1, 'L', true);

        $pdf->Ln(5);
        $pdf = $this->_getFormElementsById($pdf,$aForms);

        //Output PDF
        if($blnSavePdf) {
            $filename = date('YmdHis')."_".$this->oComp->alias.".pdf";
            $pdf->Output(dirname(\Environment::get('documentRoot')).$this->tmpSavePath.$filename,'F',true);
            return $this->tmpSavePath.$filename;
        }else {
            return new Response($pdf->Output(), 200, array(
                'Content-Type' => 'application/pdf'));
        }
    }


    /**
     * @param \DataContainer $dc
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function exportRequestData(\DataContainer $dc)
    {

        //Get Data
        $oCompRequest = CompetitionRequestModel::findBy('pid',$dc->id);
        $oComp = $oCompRequest->getRelated('pid');

        $strExportTpl = $oComp->exportTpl?? 'memo_competition_export_schoolMasterShipEast.export.xls';

        $styleCellArray = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => 'FF000000')
                ],
                'left' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => 'FF000000')
                ],
                'right' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => 'FF000000')
                ],
            ]
        ];

        if(!empty($oComp->exportRaw) && $oComp->exportRaw == 1) {
            //Export all Fields raw
            $spreadsheet = new Spreadsheet();

            //Add Logo
            $drawing = new HeaderFooterDrawing();
            $drawing->setName('Swissfaustball Logo');
            $drawing->setPath(dirname(dirname(__FILE__)).'/Resources/public/vorlagen/logo.png');
            $sheet       = $spreadsheet->getActiveSheet();

            $iLineNo = 2;
            $sheet->fromArray(array_keys(unserialize($oCompRequest[0]->formData)), NULL, 'A1');

            $tmp = unserialize($oCompRequest[0]->formData);
            if(!empty($tmp['team']) && is_array($tmp['team'])) {

                $template = new \FrontendTemplate($strExportTpl);
                $template->data = $oCompRequest;
                $template->oComp = $oComp;
                $template->basUrl = $this->request->getCurrentRequest()->getSchemeAndHttpHost();

                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Html();
                $spreadsheet = $reader->loadFromString($template->parse());
                $objActiveSheet = $spreadsheet->getSheet(0);
                $objActiveSheet->getPageSetup()->setFitToPage(1);
                $objActiveSheet->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
            }else {
                foreach ($oCompRequest as $key => $row) {
                    $aFormData = unserialize($row->formData);
                    foreach ($aFormData as $k => $v) {
                        if (is_array($v)) {
                            $aFormData[$k] = implode(',', $v);
                        }
                    }
                    $sheet->fromArray($aFormData, "", 'A' . $iLineNo);
                    $iLineNo++;
                }
            }
            //Get Filename and output to browser
            $fileName = date("Y-m-d_His_").$oComp->alias.".xls";
            $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xls");
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="'. urlencode($fileName).'"');
            $writer->save('php://output');

            \Contao\Controller::redirect('/contao?do=competitions&table=tl_competition_request');
        }

        /** Load Template to a Spreadsheet Object  **/
        $spreadsheet = IOFactory::load(dirname(dirname(__FILE__)).'/Resources/public/vorlagen/kontrollblatt_maenner.xls');
        $objActiveSheet = $spreadsheet->getSheet(0);
        $objActiveSheet->getPageSetup()->setFitToPage(1);
        $objActiveSheet->getPageSetup()->setFitToHeight(0);

        //Set Start Line No.
        $iLineNo = 4;
        foreach($oCompRequest as $key => $val) {
            $aFormData = unserialize($val->formData);
            $sTeams = empty($aFormData['nachwuchsteams']) ? '' : implode(", ", $aFormData['nachwuchsteams']);

            $objActiveSheet->getCell('A' . $iLineNo)->setValue(str_replace($val->league, "", $val->team)); //Titel
            if (stristr($val->league, 'nla') or stristr($val->league, 'Nationalliga')) {
                $objActiveSheet->getCell('B' . $iLineNo)->setValue($val->league);
            } elseif (stristr($val->league, 'nlb')) {
                $objActiveSheet->getCell('C' . $iLineNo)->setValue($val->league);
            } elseif ($val->league == '1. Liga Männer' || $val->league == '1_Liga') {
                $objActiveSheet->getCell('B' . $iLineNo)->setValue($val->league);
                $objActiveSheet->getCell('C' . $iLineNo)->setValue($aFormData['squad']);
            }elseif($val->league == '') {
                $objActiveSheet->getCell('B' . $iLineNo)->setValue('2. Liga');
                if(!empty($aFormData['squad'])) {
                    $objActiveSheet->getCell('C' . $iLineNo)->setValue($aFormData['squad']);
                }
            }else{
                $objActiveSheet->getCell('B' . $iLineNo)->setValue($val->league);
                $objActiveSheet->getCell('C' . $iLineNo)->setValue($aFormData['squad']);
            }
            $objActiveSheet->getCell('D'.$iLineNo)->setValue(sprintf("%s %s,\n%s, %s",empty($aFormData['prename'])?'': $aFormData['prename'],empty($aFormData['name'])? '':$aFormData['name'],empty($aFormData['street'])?'':$aFormData['street'],empty($aFormData['location'])?'':$aFormData['location']));
            $objActiveSheet->getCell('E'.$iLineNo)->setValue(empty($aFormData['email'])?'':$aFormData['email']);
            $objActiveSheet->getCell('F'.$iLineNo)->setValue(empty($aFormData['iban'])?'':$aFormData['iban']);
            $objActiveSheet->getCell('G'.$iLineNo)->setValue(sprintf("%s %s",empty($aFormData['coachname'])?'':$aFormData['coachname'],empty($aFormData['coachprename'])?'':$aFormData['coachprename']));
            $objActiveSheet->getCell('H'.$iLineNo)->setValue(empty($aFormData['eduationLevel'])?'':$aFormData['eduationLevel']);
            $objActiveSheet->getCell('I'.$iLineNo)->setValue(sprintf("%s %s",empty($aFormData['refereeprename'])?'':$aFormData['refereeprename'],empty($aFormData['refereehname'])?'':$aFormData['refereehname']));
            $objActiveSheet->getCell('J'.$iLineNo)->setValue($sTeams);
            $objActiveSheet->getCell('K'.$iLineNo)->setValue($aFormData['date']?? '');

            //Textwrap on cells
            $objActiveSheet->getStyle('A'.$iLineNo.":L".$iLineNo)->getAlignment()->setWrapText(true);
            //Style Row
            //Set Auto Height
            $objActiveSheet->getRowDimension($iLineNo)->setRowHeight(-1);
            foreach ($objActiveSheet->getColumnIterator() as $column) {
                $objActiveSheet->getStyle($column->getColumnIndex().$iLineNo)->applyFromArray($styleCellArray);
                //Break if last Column enter
                if($column->getColumnIndex() == "K")
                {
                    break;
                }
            }
            $iLineNo++;
        }

        //Set Autowidth
        $cellIterator = $objActiveSheet->getRowIterator()->current()->getCellIterator();
        $cellIterator->setIterateOnlyExistingCells( true );
        /** @var PHPExcel_Cell $cell */
        foreach( $cellIterator as $cell ) {
            $objActiveSheet->getColumnDimension( $cell->getColumn() )->setAutoSize( true );
        }

        //Add Logo
        $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\HeaderFooterDrawing();
        $drawing->setName('Swissfaustball Logo');
        $drawing->setPath(dirname(dirname(__FILE__)).'/Resources/public/vorlagen/logo.png');
//        $drawing->setWidth(200);
        $drawing->setHeight(36);
        $drawing->setResizeProportional(true);

//        $drawing->setWorksheet($objActiveSheet);
//        $drawing->setHyperlink('https://www.swissfaustball.ch');

        $objActiveSheet->getHeaderFooter()->addImage($drawing, \PhpOffice\PhpSpreadsheet\Worksheet\HeaderFooter::IMAGE_HEADER_LEFT);
        $objActiveSheet->getHeaderFooter()->setOddHeader('&L&G'.'&C&20'.$oComp->title);

        //Get Filename and output to browser
        $fileName = date("Y-m-d_His_").$oComp->alias.".xls";
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xls");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'. urlencode($fileName).'"');
        $writer->save('php://output');


        \Contao\Controller::redirect('/contao?do=competitions&table=tl_competition_request');

    }

    /**
     * @param array $arrFormIds
     * @param $pdf
     * @return mixed
     */
    public function _getFormElementsById($pdf,$arrFormIds = array()) {

        if(is_array($arrFormIds) and !empty($arrFormIds)) {
            foreach($arrFormIds as $key => $frmId) {
                if(count($arrFormIds)-1 == $key) {
                    $pdf = $this->_getFormById($frmId, $pdf,false);
                }else{
                    $pdf = $this->_getFormById($frmId, $pdf);
                }
            }
        }
        return $pdf;
    }

    /**
     * @param $prmFormId
     * @param $pdf
     * @return false|mixed
     */
    function _getFormById($prmFormId, $pdf,$blnAddSpacer=1) {
        if(empty($pdf)) {
            return false;
        }

        if($prmFormId == 3) {
            //Mannschaft
            $pdf->SetFont('Arial', 'B', $this->fontSize);
            $pdf->Cell($this->col1Width, 7, "Mannschaft:", 'LRTB', 0);
            $pdf->SetFont('Arial', '', $this->fontSize);
            $pdf->Cell(100, 7, utf8_decode($this->oCompRequest->team), 'LRTB', 0);
            $pdf->Cell(0, 7, "Liga: " . utf8_decode($this->oCompRequest->league), 'LRTB', 1);

        }elseif($prmFormId == 4) {
            //Verband
            $pdf->SetFont('Arial', 'B', $this->fontSize);
            $pdf->Cell($this->col1Width,7,"Turnverband:",'LRTB',0);
            $pdf->SetFont('Arial', '', $this->fontSize);
//            $pdf->Cell(50,7,utf8_decode($this->RequestData['gymnasticassociation']),'LRTB',0);
//            $pdf->Cell(50,7,utf8_decode($this->RequestData['gymnasticassociation']),'LRTB',0);
            $pdf->Cell(0,7,strtoupper(utf8_decode($this->RequestData['gymnasticassociation'])),'LRTB',1);

        }elseif($prmFormId == 5) {
            //Kontaktadresse Verein

            $sAdressType = '';
            if($this->RequestData['assoc_address_type'] == 'm') {
                $sAdressType = utf8_decode(' (Männer)');
            }elseif($this->RequestData['assoc_address_type'] == 'f') {
                $sAdressType = utf8_decode(' (Frauen)');
            }

            $pdf->SetFont('Arial', 'B', $this->fontSize);
            $cYPos = $pdf->GetY();
            $pdf->MultiCell($this->col1Width,7,"Offizielle Kontakt- adresse des Vereins"."$sAdressType:",'LRT','L');
            $pdf->SetXY(50,$cYPos);
            $pdf->SetFont('Arial', '', $this->fontSize);
            $pdf->Cell(75,7,'Name: '.utf8_decode($this->RequestData['name']),'LRTB',0);
            $pdf->Cell(75,7,'Vorname: '.utf8_decode($this->RequestData['prename']),'LRTB',1);
            $pdf->SetX(50);
            $pdf->Cell(75,7,'Strasse: '.utf8_decode($this->RequestData['street']),'LRTB',0);
            $pdf->Cell(75,7,'PLZ/Ort: '.utf8_decode($this->RequestData['location']),'LRTB',1);
            $pdf->SetX(50);
            $pdf->Cell(75,7,'Tel. P: '.utf8_decode($this->RequestData['phoneprivate']),'LRTB',0);
            $pdf->Cell(75,7,'Tel. G: '.utf8_decode($this->RequestData['phonecompany']),'LRTB',1);
            $pdf->SetX(50);
            $pdf->Cell(0,7,'Bankverbindung (IBAN): '.utf8_decode($this->RequestData['iban']),'LRTB',1);
            $cYPos = $pdf->GetY();
            $pdf->Line(10,$cYPos,50,$cYPos);
            $pdf->Line(10,$cYPos,10,$cYPos-15);


        }elseif($prmFormId == 6) {
            //Trainer Coach
            $pdf->SetFont('Arial', 'B', $this->fontSize);
            $cYPos = $pdf->GetY();
            $pdf->MultiCell($this->col1Width,7,"Trainer:",'LRT','L');
            $pdf->SetXY(50,$cYPos);
            $pdf->SetFont('Arial', '', $this->fontSize);
            $pdf->Cell(75,7,'Name: '.utf8_decode($this->RequestData['coachname']),'LRTB',0);
            $pdf->Cell(75,7,'Vorname: '.utf8_decode($this->RequestData['coachprename']),'LRTB',1);
            $pdf->SetX(50);
            $pdf->Cell(0,7,'Ausbildung: '.utf8_decode($this->RequestData['eduationLevel']),'LRTB',1);
            $cYPos = $pdf->GetY();
            $pdf->Line(10,$cYPos,50,$cYPos);
            $pdf->Line(10,$cYPos,10,$cYPos-7);

            if(isset($this->RequestData['junior_coachname']) && isset($this->RequestData['junior_coachprename']) && isset($this->RequestData['junior_eduationLevel'])) {
                $pdf->SetFont('Arial', 'B', $this->fontSize);
                $cYPos = $pdf->GetY();
                $pdf->MultiCell($this->col1Width, 7, "Nachwuchsleiter:", 'LRT', 'L');
                $pdf->SetXY(50, $cYPos);
                $pdf->SetFont('Arial', '', $this->fontSize);
                $pdf->Cell(75, 7, 'Name: ' . utf8_decode($this->RequestData['junior_coachname']), 'LRTB', 0);
                $pdf->Cell(75, 7, 'Vorname: ' . utf8_decode($this->RequestData['junior_coachprename']), 'LRTB', 1);
                $pdf->SetX(50);
                $pdf->Cell(0, 7, 'Ausbildung: ' . utf8_decode($this->RequestData['junior_eduationLevel']), 'LRTB', 1);
                $cYPos = $pdf->GetY();
                $pdf->Line(10, $cYPos, 50, $cYPos);
                $pdf->Line(10, $cYPos, 10, $cYPos - 7);
            }

        }elseif($prmFormId == 7) {
            //Schiri
            $pdf->SetFont('Arial', 'B', $this->fontSize);
            $cYPos = $pdf->GetY();
            $pdf->MultiCell($this->col1Width,7,"Schiedsrichter:",'LRT','L');
            $pdf->SetXY(50,$cYPos);
            $pdf->SetFont('Arial', '', $this->fontSize);
            $pdf->Cell(75,7,'Name: '.utf8_decode($this->RequestData['refereehname']),'LRTB',0);
            $pdf->Cell(75,7,'Vorname: '.utf8_decode($this->RequestData['refereeprename']),'LRTB',1);
            if(!empty($this->RequestData['referee2name'])) {
                $cYPos2 = $pdf->GetY();
                $pdf->SetXY(50,$cYPos2);
                $pdf->Cell(75,7,'Name: '.utf8_decode($this->RequestData['referee2name']),'LRTB',0);
                $pdf->Cell(75,7,'Vorname: '.utf8_decode($this->RequestData['referee2prename']),'LRTB',1);
            }
            if(!empty($this->RequestData['referee3name'])) {
                $cYPos2 = $pdf->GetY();
                $pdf->SetXY(50,$cYPos2);
                $pdf->Cell(75,7,'Name: '.utf8_decode($this->RequestData['referee3name']),'LRTB',0);
                $pdf->Cell(75,7,'Vorname: '.utf8_decode($this->RequestData['referee3prename']),'LRTB',1);
            }

            if(!empty($this->RequestData['referee4name'])) {
                $cYPos2 = $pdf->GetY();
                $pdf->SetXY(50,$cYPos2);
                $pdf->Cell(75,7,'Name: '.utf8_decode($this->RequestData['referee4name']),'LRTB',0);
                $pdf->Cell(75,7,'Vorname: '.utf8_decode($this->RequestData['referee4prename']),'LRTB',1);
            }
            if(!empty($this->RequestData['referee5name'])) {
                $cYPos2 = $pdf->GetY();
                $pdf->SetXY(50,$cYPos2);
                $pdf->Cell(75,7,'Name: '.utf8_decode($this->RequestData['referee5name']),'LRTB',0);
                $pdf->Cell(75,7,'Vorname: '.utf8_decode($this->RequestData['referee5prename']),'LRTB',1);
            }

            $pdf->Line(10,$cYPos,10,$pdf->GetY());
            $cYPos = $pdf->GetY();
            $pdf->Line(10,$cYPos,50,$cYPos);

        }elseif($prmFormId == 8) {
            //Nachwuchsteams
            $aNachwuchs = $this->RequestData['nachwuchsteams'];
            $pdf->SetFont('Arial', 'B', $this->fontSize);
            $cYPos = $pdf->GetY();
            $pdf->MultiCell($this->col1Width,7,"Nachwuchsteams:",'LRT','L');
            $pdf->SetXY(50,$cYPos);
            $pdf->SetFont('Arial', '', $this->fontSize);
            $tmpTxt = in_array('U10',$aNachwuchs)? 'X' : '';
            $pdf->Cell(25,7,'U10: '.$tmpTxt,'LRTB',0);
            $tmpTxt = in_array('U12',$aNachwuchs)? 'X' : '';
            $pdf->Cell(25,7,'U12: '.$tmpTxt,'LRTB',0);
            $tmpTxt = in_array('U14',$aNachwuchs)? 'X' : '';
            $pdf->Cell(25,7,'U14: '.$tmpTxt,'LRTB',0);
            $tmpTxt = in_array('U16',$aNachwuchs)? 'X' : '';
            $pdf->Cell(25,7,'U16: '.$tmpTxt,'LRTB',0);
            $tmpTxt = in_array('U18',$aNachwuchs)? 'X' : '';
            $pdf->Cell(25,7,'U18: '.$tmpTxt,'LRTB',0);
            $tmpTxt = empty($aNachwuchs)? 'X' : '';
            $pdf->Cell(0,7,'Keine: '.$tmpTxt,'LRTB',1);
            $cYPos = $pdf->GetY();
            $pdf->Line(10,$cYPos,50,$cYPos);

        }elseif($prmFormId == 9) {
            //Schiri
            $pdf->SetFont('Arial', 'B', $this->fontSize);
            $cYPos = $pdf->GetY();
            $pdf->MultiCell($this->col1Width,7,"J+S-Coach:",'LRT','L');
            $pdf->SetXY(50,$cYPos);
            $pdf->SetFont('Arial', '', $this->fontSize);
            $pdf->Cell(75,7,'Name: '.utf8_decode($this->RequestData['jscoachname']),'LRTB',0);
            $pdf->Cell(75,7,'Vorname: '.utf8_decode($this->RequestData['jscoachprename']),'LRTB',1);
            $pdf->SetX(50);
            $pdf->Cell(0,7,'E-Mail: '.utf8_decode($this->RequestData['jscoachemail']),'LRTB',1);
            $cYPos = $pdf->GetY();
            $pdf->Line(10,$cYPos,50,$cYPos);
            $pdf->Line(10,$cYPos,10,$cYPos-7);

        }elseif($prmFormId == 10) {
            //Webmaster
            $pdf->SetFont('Arial', 'B', $this->fontSize);
            $cYPos = $pdf->GetY();
            $pdf->MultiCell($this->col1Width,7,"Webmaster:",'LRT','L');
            $pdf->SetXY(50,$cYPos);
            $pdf->SetFont('Arial', '', $this->fontSize);
            $pdf->Cell(75,7,'Name: '.utf8_decode($this->RequestData['webmastername']),'LRTB',0);
            $pdf->Cell(75,7,'Vorname: '.utf8_decode($this->RequestData['webmasterprename']),'LRTB',1);
            $pdf->SetX(50);
            $pdf->Cell(0,7,'E-Mail: '.utf8_decode($this->RequestData['webmasteremail']),'LRTB',1);
            $cYPos = $pdf->GetY();
            $pdf->Line(10,$cYPos,50,$cYPos);
            $pdf->Line(10,$cYPos,10,$cYPos-7);


        }elseif($prmFormId == 11) {
            //Bemerkung
            if($pdf->GetY() > 240) {
                $cYPos = $pdf->GetY();
                $pdf->Line(10,$cYPos,$pdf->pageWidth()+10,$cYPos);
                $pdf->AddPage();
                $pdf->Ln(20);
            }
            $pdf->SetFont('Arial', 'B', $this->fontSize);
            $cYPos = $pdf->GetY();
            $pdf->MultiCell($this->col1Width,7,"Bemerkungen:",'LRT','L');
            $pdf->SetXY(50,$cYPos);
            $pdf->SetFont('Arial', '', $this->fontSize);
            $pdf->MultiCell(0,7,utf8_decode($this->RequestData['comments']),'LRT','L');
            $pdf->Line(10,$cYPos,10,$pdf->GetY());
            $cYPos = $pdf->GetY();
            $pdf->Line(10,$cYPos,$pdf->pageWidth()+10,$cYPos);

        }elseif($prmFormId == 12) {
            //Erklärung
            if($pdf->GetY() > 230) {
                $cYPos = $pdf->GetY();
                $pdf->Line(10,$cYPos,$pdf->pageWidth()+10,$cYPos);
                $pdf->AddPage();
                $pdf->Ln(20);
                $cYPos = $pdf->GetY();
                $pdf->Line(10,$cYPos,$pdf->pageWidth()+10,$cYPos);
            }
            $oFormFields = \FormFieldModel::findOneBy('pid',12);
            $pdf->SetFont('Arial', '', $this->fontSize);
            $cYPos = $pdf->GetY();
            $pdf->WriteHTML(utf8_decode(StringUtil::decodeEntities($oFormFields->text)));
            $pdf->Ln(5);
//            $pdf->Line(10,$pdf->GetY(),$pdf->pageWidth()+10,$pdf->GetY());
            $pdf->Line(10,$pdf->GetY(),10,$cYPos);
            $pdf->Line($pdf->pageWidth()+10,$pdf->GetY(),$pdf->pageWidth()+10,$cYPos);



        }elseif($prmFormId == 13) {
            //Anmeldung durch
            if($pdf->GetY() > 230) {
                $cYPos = $pdf->GetY();
                $pdf->Line(10,$cYPos,$pdf->pageWidth()+10,$cYPos);
                $pdf->AddPage();
                $pdf->Ln(20);
                $cYPos = $pdf->GetY();
                $pdf->Line(10,$cYPos,$pdf->pageWidth()+10,$cYPos);
            }
            $pdf->SetFont('Arial', 'B', $this->fontSize);
            $cYPos = $pdf->GetY();
            $cYPosB = $pdf->GetY();
            $pdf->MultiCell($this->col1Width,7,"Anmeldung durch:",'LRT','L');
            $pdf->SetXY(50,$cYPos);
            $pdf->SetFont('Arial', '', $this->fontSize);
            $pdf->Cell(75,7,'Name: '.utf8_decode($this->RequestData['sendername']),'LRTB',0);
            $pdf->Cell(75,7,'Vorname: '.utf8_decode($this->RequestData['senderprename']),'LRTB',1);
            $pdf->SetX(50);
            $pdf->Cell(0,7,'E-Mail: '.utf8_decode($this->RequestData['senderemail']),'LRTB',1);
            $pdf->SetX(50);
            $pdf->Cell(0,7,'Datum: '.date("d.m.Y",strtotime(utf8_decode($this->RequestData['date']))),'LRTB',1);
            $cYPos = $pdf->GetY();
            $pdf->Line(10,$cYPos,50,$cYPos);
            $pdf->Line(10,$cYPos,10,$cYPosB);

        }elseif($prmFormId == 14) {
            //Verein NW
            $pdf->SetFont('Arial', 'B', $this->fontSize);
            $cYPos = $pdf->GetY();
            $cYPosB = $pdf->GetY();
            $pdf->MultiCell($this->col1Width,7,"Verein:",'LRT','L');
            $pdf->SetXY(50,$cYPos);
            $pdf->SetFont('Arial', '', $this->fontSize);
            $pdf->Cell(0,7,'Verein: '.utf8_decode($this->RequestData['club']),'LRTB',1);
            $cYPos = $pdf->GetY();
            $pdf->Line(10,$cYPos,50,$cYPos);
            $pdf->Line(10,$cYPos,10,$cYPosB);

        }elseif($prmFormId == 15) {
            //Anmeldung NW-Verantwortlicher
            $pdf->SetFont('Arial', 'B', $this->fontSize);
            $cYPos = $pdf->GetY();
            $pdf->MultiCell($this->col1Width,7,"Nachwuchs Verantwortlicher:",'LRT','L');
            $pdf->SetXY(50,$cYPos);
            $pdf->SetFont('Arial', '', $this->fontSize);
            $pdf->Cell(75,7,'Name: '.utf8_decode($this->RequestData['nwname']),'LRTB',0);
            $pdf->Cell(75,7,'Vorname: '.utf8_decode($this->RequestData['nwprename']),'LRTB',1);
            $pdf->SetX(50);
            $pdf->Cell(75,7,'Strasse: '.utf8_decode($this->RequestData['nwstreet']),'LRTB',0);
            $pdf->Cell(75,7,'PLZ/Ort: '.utf8_decode($this->RequestData['nwlocation']),'LRTB',1);
            $pdf->SetX(50);
            $pdf->Cell(75,7,'Tel. P: '.utf8_decode($this->RequestData['nwphoneprivate']),'LRTB',0);
            $pdf->Cell(75,7,'Tel. G: '.utf8_decode($this->RequestData['nwphonecompany']),'LRTB',1);
            $pdf->SetX(50);
            $pdf->Cell(75,7,'Tel. M: '.utf8_decode($this->RequestData['nwmobilephone']),'LRTB',0);
            $pdf->Cell(75,7,'E-Mail: '.utf8_decode($this->RequestData['nwemail']),'LRTB',1);
            $pdf->SetX(50);
            $pdf->Cell(0,7,'Bankverbindung (IBAN): '.utf8_decode($this->RequestData['nwiban']),'LRTB',1);
            $cYPos = $pdf->GetY();
            $pdf->Line(10,$cYPos,50,$cYPos);
            $pdf->Line(10,$cYPos,10,$cYPos-21);

        }elseif($prmFormId == 16) {
            //Erklärung Cup Info
            $pdf->SetFont('Arial', '', $this->fontSize);
            $oFormFields = \FormFieldModel::findOneBy('pid',16);
            $pdf->SetFont('Arial', '', $this->fontSize);
            $cYPos = $pdf->GetY();
            $pdf->WriteHTML(utf8_decode(StringUtil::decodeEntities($oFormFields->text)));
            $pdf->Ln(7);
            $pdf->Line(10,$pdf->GetY(),$pdf->pageWidth()+10,$pdf->GetY());
            $pdf->Line(10,$pdf->GetY(),10,$cYPos);
            $pdf->Line($pdf->pageWidth()+10,$pdf->GetY(),$pdf->pageWidth()+10,$cYPos);
            $pdf->Line(10,$cYPos,$pdf->pageWidth()+10,$cYPos);

        }elseif($prmFormId == 17) {
            //CUP Mannschaft
            $aFormData = unserialize($this->oCompRequest->formData);
            if(!empty($aFormData['2liga'])) {
                $squad = $aFormData['squad_2_liga'];
            }else{
                $squad = $aFormData['squad'];
            }
            $pdf->SetFont('Arial', 'B', $this->fontSize);
            $pdf->Cell($this->col1Width, 7, "Mannschaft:", 'LRTB', 0);
            $pdf->SetFont('Arial', '', $this->fontSize);
            $pdf->Cell(100, 7, utf8_decode($squad), 'LRTB', 0);
            $pdf->Cell(0, 7, "Liga: " . utf8_decode($this->oCompRequest->league), 'LRTB', 1);

            if(!empty($this->RequestData['2liga'])) {
                $pdf->SetFont('Arial', 'B', $this->fontSize);
                $pdf->Cell($this->col1Width, 7, "2. Liga:", 'LRTB', 0);
                $pdf->SetFont('Arial', '', $this->fontSize);
                $pdf->Cell(0, 7, 'Region: '.utf8_decode($this->RequestData['region']), 'LRTB', 0);
                $pdf->Ln(7);
            }

        }elseif($prmFormId == 18) {
            //NW Kategorien
            $aNwCategories = $this->RequestData['category'];
            $pdf->SetFont('Arial', 'B', $this->fontSize);
            $cYPos = $pdf->GetY();
            $pdf->MultiCell($this->col1Width,7,"Nachwuchsteams Kategorie:",'LRT','L');
            $pdf->SetXY(50,$cYPos);
            $pdf->SetFont('Arial', '', $this->fontSize);
            foreach($aNwCategories as $key => $val) {
                $pdf->Cell(37, 14, utf8_decode($val), 'L', 0);
            }
            $pdf->Line(10,$cYPos,$pdf->pageWidth()+10,$cYPos);
            $pdf->Ln(10);
            $pdf->Line($pdf->pageWidth()+10,$cYPos,$pdf->pageWidth()+10,$pdf->GetY());
        }elseif($prmFormId == 22 || $prmFormId == 23) {

            //Mannschaft
            $pdf->SetFont('Arial', 'B', $this->fontSize);
            $pdf->Cell($this->col1Width, 7, "Schule / Verein:", 'LRTB', 0,'',true);
            $pdf->SetFont('Arial', '', $this->fontSize);
            $pdf->Cell(149, 7, utf8_decode($this->RequestData['schule_verein']), 'LRTB', 1,'',true);

            $pdf->SetFont('Arial', 'B', $this->fontSize);
            $pdf->Cell($this->col1Width, 7, "Klassenlehrperson:", 'LRT', 0,'T');
            $pdf->Cell(50, 7, 'Name / Vorname', 'LRTB', 0);
            $pdf->SetFont('Arial', '', $this->fontSize);
            $pdf->Cell(99, 7, utf8_decode($this->RequestData['lehrer_name'])." ".utf8_decode($this->RequestData['lehrer_vorname']), 'LRTB', 1);

            $pdf->Cell($this->col1Width,14,"",'LBR',0);
            $pdf->SetFont('Arial', 'B', $this->fontSize);
            $pdf->Cell(50, 7, 'Tel. Mobil', 'LRB', 0);
            $pdf->SetFont('Arial', '', $this->fontSize);
            $pdf->Cell(99, 7, utf8_decode($this->RequestData['tel_mobile']), 'LRB', 1);
            $pdf->SetX($this->col1Width+10);
            $pdf->SetFont('Arial', 'B', $this->fontSize);
            $pdf->Cell(50, 7, 'E-Mail', 'LRB', 0);
            $pdf->SetFont('Arial', '', $this->fontSize);
            $pdf->Cell(99, 7, utf8_decode($this->RequestData['lehrer_email']), 'LRB', 1);

            if(!empty($this->RequestData['lehrer_bemerkung'])) {
                $pdf->Ln(2);
                $pdf->SetFont('Arial', 'B', $this->fontSize);
                $pdf->Cell(0, 7, 'Bemerkungen', 'LTR', 1);
                $pdf->SetFont('Arial', '', $this->fontSize);
                $pdf->MultiCell(0, 5, utf8_decode($this->RequestData['lehrer_bemerkung']), 'LRB', 1);
            }

            if(!empty($this->RequestData['team'])) {
                $pdf->Ln(5);
                $x=1;
                foreach($this->RequestData['team'] as $key => $team) {
                    if(empty($team['name'])) {
                        continue;
                    }
                    $pdf->SetFont('Arial', 'B', 10);
                    $pdf->Cell($this->col1Width+9, 5, 'Teamname', 'LTRB', 0,'',true);
                    $pdf->Cell(70, 5, utf8_decode($team['name']??''), 'LTRB', 0,'',true);
                    $pdf->Cell(70, 5, utf8_decode($team['kategorie']??''), 'LTRB', 1,'',true);
                    $pdf->SetFont('Arial', '', 10);
                    for($i=0;$i < 3 ; $i++)
                    {
                        $pdf->Cell($this->col1Width+9, 5, utf8_decode($team['spieler']['type'][$i]), 'LRB', 0);
                        $pdf->Cell(140, 5, utf8_decode($team['spieler']['vorname'][$i])." ".utf8_decode($team['spieler']['name'][$i]), 'LRB', 1);
                    }
                    if($x%7 == 0){
                        $pdf->AddPage();
                        $pdf->Ln(5);
                    }else {
                        $pdf->Ln(4);
                    }
                    $x++;
                }
            }

        }

        if($blnAddSpacer) {
            $pdf->Cell(0, $this->VerticalSpacer, "", 'LR', 1);  //spacer
        }

        return $pdf;
    }

}
