<?php

namespace Memo\CompetitionBundle\Model;

class CompetitionRequestModel extends \Model
{
    /**
     * Table name
     * @var string
     **/
    protected static $strTable = 'tl_competition_request';
}
