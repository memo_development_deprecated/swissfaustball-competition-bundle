<?php

namespace Memo\CompetitionBundle\Model;

class CompetitionModel extends \Model
{
    /**
     * Table name
     * @var string
     **/
    protected static $strTable = 'tl_competition';
}
