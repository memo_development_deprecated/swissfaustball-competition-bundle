<?php

namespace Memo\CompetitionBundle\Controller;

use Contao\CoreBundle\Framework\ContaoFramework;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Memo\ModClubBundle\Model\TeamModel;
use Memo\ModClubBundle\Model\PersonModel;
use Memo\ModClubBundle\Model\ClubModel;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;


class CompetitionController extends AbstractController
{

    private $framework;

    /**
     * @param ContaoFramework $framework
     */
    public function __construct(ContaoFramework $framework)
    {
        $this->framework = $framework;
        $this->framework->initialize();
    }

    /**
     * @param $prmLeage
     * @return JsonResponse
     */
    public function getTeamsByLeague($prmLeage) : JsonResponse {
        $arrReturn = [];

        if(empty($prmLeage)) {
            return new JsonResponse($arrReturn);
        }

        $prmSeason = intval(\Input::post('season'));
        $strWhere = $category_order = '';
        switch ($prmLeage) {
            case 'Nationalliga' : $prmSeason === 1 ?  $strWhere = 'field_category = "a" or field_category = "b"' : $strWhere = 'hall_category = "a" or hall_category = "b"';
                    $category_order = 'field_category';
                    $strCat = 'a';break;

            case '1_Liga' : $prmSeason === 1 ?  $strWhere = 'field_category = "c" ' : $strWhere = 'hall_category = "c"';
                $category_order = 'hall_category';
                $strCat = 'c';break;
        }

        $db = \System::getContainer()->get('database_connection');
        $strQuery = sprintf("SELECT id,pid,name,alias,sexSelect FROM tl_mod_team WHERE %s ORDER BY %s = '%s', `sexSelect` ASC, `name`",$strWhere,$category_order,$strCat);
        $aTeams = $db->executeQuery($strQuery)->fetchAll(\PDO::FETCH_ASSOC);


        foreach($aTeams as $key => $team) {
            $tmp = $team;
            $tmp['club']   = ClubModel::findByPk($team['pid'])->name;
            $aReturn[$key] = $tmp;
        }

        $serializer = new Serializer(array(new ObjectNormalizer()), array('json' => new JsonEncoder()));
        $return =  $serializer->serialize($aReturn, 'json');

        return new JsonResponse($return);
    }


    /**
     * @param $teamID
     * @return JsonResponse
     */
    public function getTeamData($teamID): JsonResponse {

		$arrReturn = [];
        $prmSeason = NULL;

        if(empty($teamID)) {
            return new JsonResponse($arrReturn);
        }

        $prmSeason = intval(\Input::post('season'));

		$oTeam   = TeamModel::findByPk($teamID);
		$oClub   = $oTeam->getRelated('pid');
        $oPerson = PersonModel::findBy('pid', $teamID);

        $aPerson = [];

        if(!is_null($prmSeason) and $prmSeason === 1 and !empty($oTeam->field_name)) {
            $name = $oTeam->field_name;

        }elseif(!is_null($prmSeason) and $prmSeason === 0 and !empty($oTeam->hall_name)) {
            $name = $oTeam->hall_name;

        }else{
            $name = $oTeam->name;
        }

        $arrReturn['club_id'] = $oClub->id;
        $arrReturn['team_id'] = $teamID;
        $arrReturn['club'] = $oClub->name;
        $arrReturn['contactName'] = $oClub->contactName;
        $arrReturn['contactFirstname'] = $oClub->contactFirstname;
        $arrReturn['contactAddress'] = $oClub->contactAddress;
        $arrReturn['contactZipcode'] = $oClub->contactZipcode;
        $arrReturn['contactPlace'] = $oClub->contactPlace;
        $arrReturn['contactPhone'] = $oClub->contactPhone;
        $arrReturn['contactPhoneBusiness'] = $oClub->contactPhoneBusiness;
        $arrReturn['contactMobile'] = $oClub->contactMobile;
        $arrReturn['contactEmail'] = $oClub->contactEmail;

        $arrReturn['team']    = $name;
        $arrReturn['association'] = implode(",",unserialize($oClub->association));

		$oLeague = $oTeam->name;

        if(empty($oPerson)) {
            $arrReturn['coach'] = [];
            $arrReturn['staff'] = [];
            $arrReturn['player'] = [];
        } else {
            foreach ($oPerson as $key => $value) {
                if ($value->function == "Trainer/Coach") {
                    $arrReturn['coach'] = $value->row();
                } else {
                    $arrReturn[$value->role][] = $value->row();
                }
            }
        }

		return new JsonResponse($arrReturn);
	}
}
