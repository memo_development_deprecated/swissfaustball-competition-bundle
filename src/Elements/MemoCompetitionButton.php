<?php


/**
 * Contao Open Source CMS
 *
 * Copyright (c) Media Motion AG
 *
 * @package   SelectLineBundle
 * @author	Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


namespace Memo\CompetitionBundle\Elements;

use Memo\CompetitionBundle\Model\CompetitionModel;

class MemoCompetitionButton	 extends \ContentElement
{

    protected $strTemplate = 'ce_memo_competition_button';

    /**
     * Displays a wildcard in the back end.
     * @return string
     */
    public function generate()
    {

        if (TL_MODE == 'BE') {

//            $template = new \BackendTemplate('be_wildcard');
//            $template->wildcard = sprintf('<%s>%s</%s>',$this->hl,$this->headline,$this->hl).'### MEMO Event List:'.$this->itemCategories.' ###';
//            return $template->parse();

        }

        return parent::generate();
    }


    protected function compile()
    {
        // TODO: Implement compile() method.
		$oCompetition = CompetitionModel::findByPk($this->competition);

		$this->Template->competition  = $oCompetition;
        $this->Template->sCssID       = trim($this->cssID[0]);
        $this->Template->sCssClass    = $this->cssID[1];
    }
}
