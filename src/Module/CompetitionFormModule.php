<?php

namespace Memo\CompetitionBundle\Module;

use Contao\System;
use Haste\Util\StringUtil;
use Memo\CompetitionBundle\Model\CompetitionModel;
use Memo\CompetitionBundle\Model\CompetitionRequestModel;
use Memo\CompetitionBundle\Service\ExportService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Memo\ModClubBundle\Model\TeamModel;
use Memo\ModSwissfaustballBundle\Model\SfSeasonModel;
use Contao\Module;


class CompetitionFormModule extends Module
{

    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'mod_competition_form';

	var $oTeams;
	var $blnAddTeamField = false;
    var $seasonSelect;


    /**
     * @return string
     */
    public function generate()
    {
        if (TL_MODE == 'BE')
        {
            $objTemplate = new \Contao\BackendTemplate('be_wildcard');
            $objTemplate->wildcard = $strWildcard = 'An dieser Stelle erscheinen im Frontend das Wettbewerb Anmelde - Formular.';
            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;

            return $objTemplate->parse();
        }
        return parent::generate();
    }


    /**
     * Generate Frontend Output
     */
    protected function compile()
    {
        if (TL_MODE === 'FE')
        {
            $prmCompetitionID = \Input::post('competition');
            if(empty($prmCompetitionID)) {
                $prmCompetitionID = \Input::get('competition');
            }
            if(!empty($this->competition)){
                $prmCompetitionID = $this->competition;
            }

            if(empty($prmCompetitionID))
            {
                return false;
            }

            $oCompetition = CompetitionModel::findByPk($prmCompetitionID);
            $aForms = unserialize($oCompetition->formSelect);

            $objForm = new \Haste\Form\Form('competitionForm', 'POST', function($objHaste) {
                return \Input::post('FORM_SUBMIT') === $objHaste->getFormId();
            });

            $objForm->addFormField('competition', array(
                'name' => 'competition',
                'inputType' => 'hidden',
                'ignoreModelValue' => true,
                'value' => $prmCompetitionID
            ));

            $objForm->bindModel($oCompetition);

            // No Form elements selected
            if(empty($aForms))
            {
                return false;
            }

            //Get Teams
            $oTeams = NULL;
            $aLeagues =  unserialize($oCompetition->leagueSelect);
            $this->seasonSelect = intval($oCompetition->seasonSelect);
            $GroupSelector = $this->seasonSelect == 1? 'field_category' : 'hall_category';

            if(!empty($aLeagues)) {
                //Get Season
                if($oCompetition->seasonSelect == 1) { //Feld
                    $this->oTeams = TeamModel::findBy(['(field_category IN ("'.implode("\",\"",$aLeagues).'"))', 'sexSelect=?', 'field_active = ?'], [$oCompetition->sex, 1], ['order' => $GroupSelector.',name']);
                }else { //Halle
                    $this->oTeams = TeamModel::findBy(['(hall_category IN ("'.implode("\",\"",$aLeagues).'"))', 'sexSelect=?', 'hall_active =?'], [$oCompetition->sex, 1], ['order' => $GroupSelector.',name']);
                }
            }

            $objForm->addFormField('season', array(
                'name' => 'season',
                'inputType' => 'hidden',
                'ignoreModelValue' => true,
                'value' => $this->seasonSelect
            ));

            //Add form Fields from Form Generator
            foreach($aForms as $key => $value) {

	            $objForm->addFieldsFromFormGenerator($value, function(&$strField, &$arrDca) {
                    $iCount = 0;

                    if(preg_match('/\[\]/',$arrDca['name'])) {
                        return true;
                    }

                    $GroupSelector = $this->seasonSelect == 1? 'field_category' : 'hall_category';
		            if($strField === 'squad')
		            {
			            $aOptions = unserialize($arrDca['options']);
			            //Fill Teams Option
			            if($this->oTeams)
			            {
                            $cTeamGroup = NULL;
				            foreach($this->oTeams as $key => $val)
				            {
                                if($val->$GroupSelector != $cTeamGroup) {
                                    $cTeamGroup = $val->$GroupSelector;
                                    if($cTeamGroup == 'a') {$sGroupe =  'Nationalliga A';}elseif($cTeamGroup == 'b') {$sGroupe = 'Nationalliga B';}else {$sGroupe = '1. Liga';}
                                    $aOptions[] = ['label'=>sprintf('----- %s -----',$sGroupe),'group'=>1,'value'=>''];
                                }
                                $oParent = $val->getRelated('pid');
                                if(!empty($val->field_name) and $this->seasonSelect === 1) {
                                    $name = $val->field_name;
                                }elseif(!empty($val->hall_name)) {
                                    $name = $val->hall_name;
                                }else{
                                    $name = $val->name;
                                }
					            $aOptions[] = ['value'=>$val->id,'label'=>$oParent->name." – ".$name];
				            }

							$arrDca['options'] = serialize($aOptions);
                            $arrDca['eval']['isAssociative'] = true;

						}else{
							$arrDca['type'] = 'text';
		            	}
		            }elseif($strField === 'date') {
                        $arrDca['value'] = date("d.m.Y",time());
                    }
	                // you must return true otherwise the field will be skipped
	                return true;
	            });
            }

            //Add Submit Button
            $objForm->addSubmitFormField('submit', 'Formular senden');
            $objForm->addCaptchaFormField('captcha');

            if ($objForm->validate()) {

                // Get all the submitted and parsed data (only works with POST):
                $arrData  = $objForm->fetchAll();
                $clubName = empty($arrData['club'])? "" : trim($arrData['club']);
                $oTeam    = null;
                $oClub    = null;

                if(empty($clubName) && !empty($arrData['schule_verein'])){
                    $clubName = $arrData['schule_verein'];
                }

                if(isset($arrData['squad'])) {
                    $oTeam = TeamModel::findByPk($arrData['squad']);
                }

                if(!is_null($oTeam)) {
                    $oClub = $oTeam->getRelated('pid');
                    $clubName = $oClub->name;
                }

                //Save Entry
                $oCompRequest = new CompetitionRequestModel();
                $oCompRequest->pid      = $arrData['competition'];
                $oCompRequest->tstamp   = time();
                if(!empty($oTeam->name)) {
                    $oCompRequest->team = $clubName . " " . $oTeam->name;
                }else{
                    $oCompRequest->team = $clubName;
                }
                $oCompRequest->league   = empty($arrData['league'])? '' : $arrData['league'];

                //Special Hack für Schulmeisterschaft
                if(!empty($arrData['team[0][name]'])){
                    $arrData['team'] = $_POST['team'];
                    $arrData['senderprename'] = $arrTokens['form_lehrer_vorname'] = $arrData['lehrer_vorname'];
                    $arrData['sendername']  = $arrTokens['form_lehrer_name'] = $arrData['lehrer_name'];
                    $arrData['senderemail'] = $arrTokens['form_lehrer_email'] = $arrData['lehrer_email'];
                    $arrTokens['form_lehrer_tel_mobile'] = $arrData['tel_mobile']?? '';
                    $arrTokens['form_schule_verein'] = $arrData['schule_verein']?? '';
                    $arrTokens['form_lehrer_email'] = $arrData['lehrer_email'];
                    $arrTokens['form_lehrer_bemerkung'] = $arrData['lehrer_bemerkung'];
                }

                $oCompRequest->formData = serialize($arrData);
                $oCompRequest->save();


                $arrTokens['form_title'] = $oCompetition->title;
                if(!empty($oTeam->name) && !empty($oClub->name)) {
                    $arrTokens['form_squad'] = $oClub->name . " " . $oTeam->name;
                }elseif(empty($oClub->name) && !empty($oTeam->name)){
                    $arrTokens['form_squad'] = $oTeam->name;
                }
                $arrTokens['form_senderprename'] = $arrData['senderprename']?? '';
                $arrTokens['form_sendername']    = $arrData['sendername']?? '';
                $arrTokens['form_senderemail']   = $arrData['senderemail']?? '';
                $arrTokens['form_date']          = $arrData['date']?? time();
                $aRecepients    = explode(";",$oCompetition->formReceiver);

                //Generate PDF Form
                $oExport = \System::getContainer()->get('Memo\CompetitionBundle\Service\ExportService');

                $arrTokens['form_request_pdf'] = $oExport->exportRequestPDF(NULL,$oCompRequest->id,1);

                //Send Notification

                if(!empty($this->notification) and is_numeric($this->notification)) {
                    $oNotification = \NotificationCenter\Model\Notification::findByPk($this->notification);
                    if (null !== $oNotification) {
                        foreach($aRecepients as $key => $val) {
                            $arrTokens['form_recipients'] = $val;
                            $oNotification->send($arrTokens);
                        }
                    }
                }


                //Redirect to OK-Page
                if($oCompetition->jumpTo != 0) {
                    $urlGenerator = \System::getContainer()->get('contao.routing.url_generator');
                    $oOkPage = \PageModel::findById($oCompetition->jumpTo);
                    \Controller::redirect($urlGenerator->generate($oOkPage->alias));
                }else{
                    $objTemplate = new \FrontendTemplate('modules/mod_competition_form_ok');
                    $objTemplate->setData(['competition' => $oCompetition,'frmData'=>$arrData]);
                    $this->Template = $objTemplate;
                }
            }

		    $GLOBALS['TL_JAVASCRIPT'][]     = '/bundles/competition/js/competition-form.js';
		    $GLOBALS['TL_CSS'][]            = '/bundles/competition/css/competition-form.css|static';
            $this->Template->competition    = $oCompetition;
            $this->Template->form = $objForm->generate();

        }
    }
}
