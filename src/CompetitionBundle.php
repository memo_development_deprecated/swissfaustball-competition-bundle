<?php

/**
 * @package   Competition Bundle
 * @author    Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\CompetitionBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class CompetitionBundle extends Bundle
{

}
